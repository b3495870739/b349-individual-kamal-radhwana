﻿using System.Collections;
using System.ComponentModel.DataAnnotations;

namespace AssetManagement.Models
{
    public class Product
    {
        [Key]
        [Required]
        public int Id { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 5)]
        [Display(Name = "Product Name")]
        public string productName { get; set; }

        [Required]
        public Category Category { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public double Price { get; set; }

        [Required]
        [DataType(DataType.ImageUrl)]
        [Display(Name = "Product Image")]
        public string productImg { get; set; } = null!;

    }
}
