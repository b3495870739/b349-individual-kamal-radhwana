﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace AssetManagement.Models
{
    public enum Category
    {
        Food,
        Electronics,
        Clothing,
        Furniture
    }
}