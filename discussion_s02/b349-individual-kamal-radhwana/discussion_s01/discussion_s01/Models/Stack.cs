﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace discussion_s01.Models
{
    public enum Stack
    {
        MEAN,
        MERN,
        Django,
        [Display(Name = "Ruby on Rails")] Rails,
        LAMP
    }
}