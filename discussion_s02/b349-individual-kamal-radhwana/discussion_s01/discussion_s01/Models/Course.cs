﻿using System.Collections;
using System.ComponentModel.DataAnnotations;

namespace discussion_s01.Models
{
    public class Course
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Title is Required!")]
        [StringLength(50, MinimumLength = 10)]
        public string Title { get; set; } = null!;

        [Required]
        public Stack Stack { get; set; }

        [Required]
        public string Description { get; set; } = null!;

        [Required]
        [DataType(DataType.Currency)]
        public double Price { get; set; }

        [Required]
        [DataType(DataType.Url)]
        public string StackUrl { get; set; } = null!;

        [Required]
        [DataType(DataType.ImageUrl)]
        [Display(Name = "Logo")]
        public string Logo { get; set; } = null!;
    }
}